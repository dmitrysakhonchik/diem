package by.sakhonchik.diem.entities;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class NginxStatus {

    private Integer activeConnections;
    private Map<String, Integer> requests;


}
