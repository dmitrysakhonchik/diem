package by.sakhonchik.diem.controllers;

import by.sakhonchik.diem.entities.NginxStatus;
import by.sakhonchik.diem.services.NginxStatusService;
import by.sakhonchik.diem.services.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class NginxStatusController {
    private final NginxStatusService nginxStatusService;
    private final ResponseService responseService;

    @Autowired
    public NginxStatusController(NginxStatusService service, ResponseService responseService) {
        this.nginxStatusService = service;
        this.responseService = responseService;
    }

    @GetMapping("/nginx/statistics")
    public NginxStatus getNginxStatus() {
        String response = responseService.getResponseBodyFromNginx();
        return nginxStatusService.getStatus(response);
    }


}
