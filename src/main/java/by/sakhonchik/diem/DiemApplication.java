package by.sakhonchik.diem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiemApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiemApplication.class, args);
    }

}
