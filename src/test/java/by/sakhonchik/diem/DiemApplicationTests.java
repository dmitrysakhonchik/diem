package by.sakhonchik.diem;

import by.sakhonchik.diem.entities.NginxStatus;
import by.sakhonchik.diem.services.NginxStatusService;
import by.sakhonchik.diem.services.ResponseService;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DiemApplicationTests {

    ResponseService responseService;
    NginxStatusService nginxStatusService;

    @Before
    public void initService() {
        nginxStatusService = new NginxStatusService();
        responseService = new ResponseService();
    }

    @Test
    public void statusShouldBeNotNull() {

        NginxStatus nginxStatus = nginxStatusService.getStatus("1 2 3 4 5 6 7 8 9 10");
        Assume.assumeNotNull(nginxStatus);
    }

    @Test
    public void responseShouldBeNotNull() {
        String response = responseService.getResponseBodyFromNginx();
        Assert.assertNotNull(response);

    }

    @Test
    public void responseShouldByEmpty() {
        String expected = "";
        String actual = responseService.getResponseBodyFromNginx();
        Assert.assertEquals(expected, actual);
    }

}
