FROM openjdk:11
VOLUME /tmp
ADD target/diem-1.0-SNAPSHOT.jar diem-1.0-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","diem-1.0-SNAPSHOT.jar"]
