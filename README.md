Spring boot application to monitor the status of the nginx server.

- git clone https://gitlab.com/dmitrysakhonchik/diem.git
- in the root of the folder execute docker-compose up
- you can get statistics at http://localhost/nginx/statistics    